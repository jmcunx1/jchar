## jchar - UTF-8 test print

All this does is print a small list of UTF-8 Characters.
I would then copy a character from the print to use in a
document or a chat.  I created it be cause I never remember
the key combination to type a non-ASCII UTF-8 Character.

It can also be used to test your terminal to see if it
can handle some common UTF-8 Characters.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jchar) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jchar.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jchar.gmi (mirror)

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
