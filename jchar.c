/*
 * Copyright (c) 2016 ... 2024 2025
 *     John McCue
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/*
 * print some common UTF-8 Characters to test terminal
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * fractions()
 */
void fractions(void)
{
  fprintf(stdout, "\n");
  fprintf(stdout, "    1/10      %c%c%c       2/5       %c%c%c\n",     0xE2, 0x85, 0x92,   0xE2, 0x85, 0x96);
  fprintf(stdout, "    1/9       %c%c%c       1/2       %c%c\n"  ,     0xE2, 0x85, 0x91,   0xC2, 0xBD      );
  fprintf(stdout, "    1/8       %c%c%c       3/5       %c%c%c\n",     0xE2, 0x85, 0x9B,   0xE2, 0x85, 0x97);
  fprintf(stdout, "    1/7       %c%c%c       5/8       %c%c%c\n",     0xE2, 0x85, 0x90,   0xE2, 0x85, 0x9D);
  fprintf(stdout, "    1/6       %c%c%c       2/3       %c%c%c\n",     0xE2, 0x85, 0x99,   0xE2, 0x85, 0x94);
  fprintf(stdout, "    1/5       %c%c%c       3/4       %c%c\n"  ,     0xE2, 0x85, 0x95,   0xC2, 0xBE      );
  fprintf(stdout, "    1/4       %c%c       4/5       %c%c%c\n",     0xC2, 0xBC      ,   0xE2, 0x85, 0x98);
  fprintf(stdout, "    1/3       %c%c%c       5/6       %c%c%c\n",     0xE2, 0x85, 0x93,   0xE2, 0x85, 0x9A);
  fprintf(stdout, "    3/8       %c%c%c       7/8       %c%c%c\n",     0xE2, 0x85, 0x9C,   0xE2, 0x85, 0x9E);

} /* fractions() */

/*
 * main()
 */
int main(void)
{

#ifdef OpenBSD
  if(pledge("stdio",NULL) == -1)
    err(1,"pledge\n");
#endif

  fprintf(stdout, "\nCommon UTF-8 Characters:\n");
  fprintf(stdout, "    Degree    %c%c       dectab    %c%c%c\n", 0xC2, 0xB0, 0xE2, 0xA8, 0xBD);
  fprintf(stdout, "    +=        %c%c       (c)       %c%c\n",   0xC2, 0xB1, 0xC2, 0xA9);
  fprintf(stdout, "    div       %c%c       (r)       %c%c\n",   0xC3, 0xB7, 0xC2, 0xAE);
  fprintf(stdout, "    cent      %c%c       (t)       %c%c%c\n", 0xC2, 0xA2, 0xE2, 0x84, 0xA2);
  fprintf(stdout, "    raise 2   %c%c       raise 3   %c%c\n",   0xC2, 0xB2, 0xC2, 0xB3);

  fractions();

  fprintf(stdout, "\nCompiled on: %s %s\n", __DATE__, __TIME__);
#ifdef OSTYPE
  fprintf(stdout,"\t%s\n",OSTYPE);
#endif  /* OSTYPE  */

  exit(EXIT_SUCCESS);
} /* main() */
